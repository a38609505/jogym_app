class KickBoardEndRequest {
  double endPosX;
  double endPosY;

  KickBoardEndRequest(this.endPosX, this.endPosY);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['endPosX'] = this.endPosX;
    data['endPosY'] = this.endPosY;

    return data;
  }
}