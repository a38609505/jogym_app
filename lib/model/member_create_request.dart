class MemberCreateRequest {
  String storeMember;
  String name;
  String phoneNumber;
  String address;
  String gender;
  String dateBirth;
  String memo;

  MemberCreateRequest(this.storeMember, this.name, this.phoneNumber, this.address, this.gender, this.dateBirth, this.memo);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['storeMember'] = storeMember;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['address'] = address;
    data['gender'] = gender;
    data['dateBirth'] = dateBirth;
    data['memo'] = memo;

    return data;
  }
}