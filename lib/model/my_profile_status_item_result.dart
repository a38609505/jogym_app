import 'package:jo_gym_app/model/my_profile_status_item.dart';

class MyProfileStatusItemResult {
  int code;
  bool isSuccess;
  String msg;
  MyProfileStatusItem data;

  MyProfileStatusItemResult(this.code, this.isSuccess, this.msg, this.data);

  factory MyProfileStatusItemResult.fromJson(Map<String, dynamic> json) {
    return MyProfileStatusItemResult(
      json['code'],
      json['isSuccess'],
      json['msg'],
      MyProfileStatusItem.fromJson(json['data']),
    );
  }
}