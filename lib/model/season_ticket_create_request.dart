class SeasonTicketCreateRequest {
  String dateCreate;
  String storeMember;
  String ticketType;
  String ticketName;
  String maxMonth;
  String unitPrice;

  SeasonTicketCreateRequest(this.dateCreate, this.storeMember, this.ticketType, this.ticketName, this.maxMonth, this.unitPrice);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['dateCreate'] = dateCreate;
    data['storeMember'] = storeMember;
    data['ticketType'] = ticketType;
    data['ticketName'] = ticketName;
    data['maxMonth'] = maxMonth;
    data['unitPrice'] = unitPrice;

    return data;
  }
}