import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/pages/member/page_member.dart';
import 'package:jo_gym_app/pages/member/page_member_buy_history.dart';
import 'package:jo_gym_app/pages/member/page_member_create.dart';
import 'package:jo_gym_app/pages/member/page_member_info_detail.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/pages/page_login.dart';
import 'package:jo_gym_app/pages/page_main.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_buy_history.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_change.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_create.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_list.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_use_history.dart';
import 'package:jo_gym_app/pages/season/page_season_buy_history.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket_change.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket_list.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket_use_history.dart';
import 'package:jo_gym_app/pages/trainer/page_trainer_member.dart';
import 'package:jo_gym_app/pages/trainer/page_trainer_member_create.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'JO GYM',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: colorBackGround,
      ),
      //home: LoginCheck(),         // 로그인 체크 - 완료
      home: PageSeasonBuyHistory(),       // 로그인 체크 - 완료
    );
  }
}
// 폰으로 했을 때 문제있는것.
// PageMember() - 완료
// PageMemberBuyHistory() - 정보를 담는 박스 크기만 수정..
// PageMemberInfoDetail() --> 완료
// PagePtTicket() - 정보를 담는 박스 크기만 수정..
// PagePtTicketBuyHistory - 정보를 담는 박스 크기만 수정..
// PagePtTicketList - 정보를 담는 박스 크기만 수정..
// PagePtTicketUseHistory --> 완료
// PageSeasonBuyHistory --> 정보를 담는 박스 크기와 일일권을 담고있는 박스 수정이 필요..
// PageSeasonTicket --> 사진만 수정 하면 됨..
// PageSeasonTicketList --> 사진만 수정 하면 됨..
// PageSeasonTicketUseHistory --> 완료
// PageTrainerMember --> 사진만 수정 하면 됨..