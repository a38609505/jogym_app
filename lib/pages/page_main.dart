import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PageMain extends StatelessWidget {
  const PageMain({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "로그인 페이지"),
        body: Column(
          children: [
            Container(
              child: Center(
                child: Column(
                  children: [
                    const ComponentMarginVertical(enumSize: EnumSize.bigger),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "회원",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "트레이너",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "정기권",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "PT권",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "정산",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "로그아웃",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
    );
  }
}
