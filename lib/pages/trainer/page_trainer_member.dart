import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PageTrainerMember extends StatelessWidget {
  const PageTrainerMember({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "직원 정보 관리"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(
                  height: 50,
                ),
                Container(
                  child: Container(
                    margin: EdgeInsets.only(left: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(color: Colors.black, width: 1),
                    ),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ComponentMarginHorizon(enumSize: EnumSize.mid),
                        Icon(Icons.search),
                        ComponentMarginHorizon(enumSize: EnumSize.mid),
                        Text(
                          "직원 검색",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w300,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ), // 회원검색 박스
                ),
                SizedBox(
                  width: 140,
                ),
                ComponentTextBtn('+ 신규 직원 등록', () =>
                    Navigator.push(context, MaterialPageRoute(
                        builder: (BuildContext context) => PageLogin())),
                  bgColor: Colors.blue,
                  borderColor: Colors.black,),
              ],
            ),
            const ComponentMarginVertical(
              enumSize: EnumSize.micro,
            ),
            Container(
                padding: bodyPaddingLeftRight,
                width: 400,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "직원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "로니콜먼",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 50),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            Container(
                padding: bodyPaddingLeftRight,
                width: 400,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "2",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "직원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "로니콜먼",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 50),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            Container(
                padding: bodyPaddingLeftRight,
                width: 400,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "3",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "직원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "로니콜먼",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 50),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
          ],
        ),
      ),

    );
  }
}
