import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/model/trainer_member_create_request.dart';
import 'package:jo_gym_app/pages/page_main.dart';
import 'package:jo_gym_app/repository/repo_trainer_member.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PageTrainerMemberCreate extends StatefulWidget {
  const PageTrainerMemberCreate({super.key});

  @override
  State<PageTrainerMemberCreate> createState() => _PageTrainerMemberCreateState();
}

class _PageTrainerMemberCreateState extends State<PageTrainerMemberCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  String? option;

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  var maskDateBirth = MaskTextInputFormatter(
      mask: '##-##-##',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  Future<void> _setTrainerMember(TrainerMemberCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoTrainerMember().setTrainerMember(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '직원등록 완료',
        subTitle: '직원등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '직원등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '직원 등록'),
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingAll,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: StyleFormDecoration().getInputDecoration('이름'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'phoneNumber',
                    decoration: StyleFormDecoration().getInputDecoration('연락처'),
                    maxLength: 13,
                    inputFormatters: [maskPhoneNumber],
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)),
                      FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    obscureText: true,
                    name: 'address',
                    decoration: StyleFormDecoration().getInputDecoration('주소'),
                    maxLength: 100,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(100, errorText: formErrorMaxLength(100)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDropdown<String>(
                    name: 'gender',
                    decoration: const InputDecoration(
                      label: Text('성별'),
                    ),
                    onChanged: (value) {
                      setState(() {
                        option = value;
                      });
                    },
                    items: const [
                      DropdownMenuItem(value: "MALE", child: Text('남성')),
                      DropdownMenuItem(value: "FEMALE", child: Text('여성')),
                    ],
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'dateBirth',
                    decoration:
                    StyleFormDecoration().getInputDecoration('생년월일'),
                    maxLength: 10,
                    inputFormatters: [maskDateBirth],
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(10, errorText: formErrorMinLength(10)),
                      FormBuilderValidators.maxLength(10, errorText: formErrorMaxLength(10)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'careerContent',
                    decoration:
                    StyleFormDecoration().getInputDecoration('경력 및 자격사항'),
                    maxLength: 10,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(1, errorText: formErrorMinLength(1)),
                      FormBuilderValidators.maxLength(200, errorText: formErrorMaxLength(200)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'memo',
                    decoration:
                    StyleFormDecoration().getInputDecoration('비고'),
                    maxLength: 200,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(1, errorText: formErrorMinLength(1)),
                      FormBuilderValidators.maxLength(200, errorText: formErrorMaxLength(200)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn('완료', () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      TrainerMemberCreateRequest request = TrainerMemberCreateRequest(
                        _formKey.currentState!.fields['name']!.value,
                        _formKey.currentState!.fields['phoneNumber']!.value,
                        _formKey.currentState!.fields['address']!.value,
                        _formKey.currentState!.fields['gender']!.value,
                        _formKey.currentState!.fields['dateBirth']!.value,
                        _formKey.currentState!.fields['careerContent']!.value,
                        _formKey.currentState!.fields['memo']!.value,
                      );
                      _setTrainerMember(request);
                    }
                  }),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
