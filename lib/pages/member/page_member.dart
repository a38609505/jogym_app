import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/common/component_text_icon_full_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/member/page_member_create.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PageMember extends StatelessWidget {
  const PageMember({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: "회원 정보 관리"),
      body:
      SingleChildScrollView(
        padding: bodyPaddingLeftRight,
        child: Column(
          children: [
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(color: Colors.black, width: 1),
                    ),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ComponentMarginHorizon(enumSize: EnumSize.mid),
                        Icon(Icons.search),
                        ComponentMarginHorizon(enumSize: EnumSize.big),
                        Text(
                          "회원 검색",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w300,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ), // 회원검색 박스
                ),
                ComponentTextBtn(
                  '+ 신규 회원 등록',
                      () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const PageMemberCreate())),
                  bgColor: Colors.blue,
                  borderColor: Colors.black,
                ),
              ],
            ), //신규 회원 등록 박스
            const ComponentMarginVertical(
              enumSize: EnumSize.small,
            ),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "등록일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2023-05-05",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "회원명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "김뚱땡",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "연락처",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "010-0000-0000",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                    // 사진은 아직 더 수정이 필요... mainAxisAlignment: MainAxisAlignment.spaceBetween,을 줘도 하기 어려움.. 알려주세요...
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
