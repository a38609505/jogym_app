import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/member/page_member_create.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket.dart';

class PageMemberInfoDetail extends StatelessWidget {
  const PageMemberInfoDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '회원 상세 정보 관리'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                      width: 200,
                      height: 200,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  const ComponentMarginVertical(),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 0.5),
                      color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "등록일",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                                "2023-05-05",
                              style: TextStyle(
                                fontSize: fontSizeMid,
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 0.5),
                        color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "회원명",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "김뚱땡",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.5),
                        color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "연락처",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "010-0000-0001",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.5),
                        color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "주소",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "안산시 단원구 중앙동",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.5),
                        color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "성별",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "남자가 되기 위해 노력중",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.5),
                        color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "생년월일",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "2005.04.12",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.5),
                        color: Color.fromRGBO(196, 190, 190, 100)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "비고",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          margin: EdgeInsets.only( right: 38),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const ComponentMarginVertical(enumSize: EnumSize.mid,),
                  Center(
                    child: Container(
                      width: 400,
                      height: 55,
                      padding: bodyPaddingLeftRight,
                      child: ComponentTextBtn(
                        '수정 / 저장',
                            () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageMemberCreate()),
                        ),
                        bgColor: Color.fromRGBO(84, 89, 87, 100),
                        borderColor: Colors.white,
                      ),
                    ),
                  ),
                  const ComponentMarginVertical(enumSize: EnumSize.small,),
                  Center(
                    child: Container(
                      width: 400,
                      height: 55,
                      padding: bodyPaddingLeftRight,
                      child: ComponentTextBtn(
                        '회원권 구매',
                            () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageSeasonTicket()),
                        ),
                        bgColor: Color.fromRGBO(130, 163, 129, 100),
                        borderColor: Colors.white,
                      ),
                    ),
                  ),
                  const ComponentMarginVertical(enumSize: EnumSize.small,),
                  Center(
                    child: Container(
                      width: 400,
                      height: 55,
                      padding: bodyPaddingLeftRight,
                      child: ComponentTextBtn(
                        '구매이력',
                            () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageSeasonTicket()),
                        ),
                        bgColor: Color.fromRGBO(84, 89, 87, 100),
                        borderColor: Colors.white,
                      ),
                    ),
                  ),
                  const ComponentMarginVertical(enumSize: EnumSize.small,),
                  Center(
                    child: Container(
                      width: 400,
                      height: 55,
                      padding: bodyPaddingLeftRight,
                      child: ComponentTextBtn(
                        '탈퇴',
                            () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageSeasonTicket()),
                        ),
                        bgColor: Color.fromRGBO(84, 89, 87, 100),
                        borderColor: Colors.white,
                      ),
                    ),
                  ),
                ],
              )
            ),
          ],
        ),
      ),
    );
  }
}
