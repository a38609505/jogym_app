import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PageMemberBuyHistory extends StatelessWidget {
  const PageMemberBuyHistory({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: "회원권 구매 내역"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // Container 사이를 띄우는 문법
              children: [
                Container(
                  width: 180,
                  padding: bodyPaddingLeftRight,
                  child: ComponentTextBtn(
                    '정기권',
                    () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => const PageLogin()),
                    ),
                    bgColor: const Color.fromRGBO(49, 176, 121, 100),
                    borderColor: Colors.white,
                  ),
                ),
                const ComponentMarginHorizon(enumSize: EnumSize.mid),
                Container(
                  width: 180,
                  padding: bodyPaddingLeftRight,
                  child: ComponentTextBtn(
                    'PT권',
                    () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => const PageLogin()),
                    ),
                    bgColor: const Color.fromRGBO(49, 176, 121, 100),
                    borderColor: Colors.white,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(
              enumSize: EnumSize.small,
            ),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "1",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "일일권명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "일일권",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "시작일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "종료일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "잔여일수",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "0",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "최근방문일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "일일권명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "일일권",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "시작일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "종료일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "잔여일수",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "0",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "최근방문일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 50,
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 2),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 200,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: const Text(
                                "No",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Container(
                              child: const Text(
                                "3",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: const Text(
                                "일일권명",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Container(
                              child: const Text(
                                "일일권",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: const Text(
                                "시작일",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Container(
                              child: const Text(
                                "2020-01-01",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: const Text(
                                "종료일",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Container(
                              child: const Text(
                                "2020-01-01",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: const Text(
                                "잔여일수",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Container(
                              child: const Text(
                                "0",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: const Text(
                                "최근방문일",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Container(
                              child: const Text(
                                "2020-01-01",
                                style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 100,
                    child: Image.asset("assets/ulgen.png"),
                  ),
                ],
              ),
            ),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "4",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "일일권명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "일일권",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "시작일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "종료일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "잔여일수",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "0",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "최근방문일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 200,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "5",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "일일권명",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "일일권",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "시작일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "종료일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "잔여일수",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "0",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: const Text(
                                  "최근방문일",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text(
                                  "2020-01-01",
                                  style: TextStyle(
                                    fontSize: fontSizeMid,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      width: 100,
                      height: 100,
                      child: Image.asset("assets/ulgen.png"),
                    ),
                  ],
                )),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
          ],
        ),
      ),
    );
  }
}
