import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/model/pt_ticket_create_request.dart';
import 'package:jo_gym_app/pages/page_main.dart';
import 'package:jo_gym_app/repository/repo_pt_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PagePtTicketCreate extends StatefulWidget {
  const PagePtTicketCreate({super.key});

  @override
  State<PagePtTicketCreate> createState() => _PagePtTicketCreateState();
}

class _PagePtTicketCreateState extends State<PagePtTicketCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  String? option;

  Future<void> _setPtTicket(PtTicketCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPtTicket().setPtTicket(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: 'PT 등록 완료',
        subTitle: 'PT 등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: 'PT 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: 'Pt 정보 등록'),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingAll,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDateTimePicker(
                    name: "dateCreate",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    decoration: StyleFormDecoration().getDateBoxDecoration("등록일시"),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'storeMember',
                    decoration: StyleFormDecoration().getInputDecoration('가맹점'),
                    maxLength: 13,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'trainerMember',
                    decoration:
                    StyleFormDecoration().getInputDecoration('직원'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'ticketName',
                    decoration:
                    StyleFormDecoration().getInputDecoration('pt권명'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'maxCount',
                    decoration:
                    StyleFormDecoration().getInputDecoration('총 횟수'),
                    maxLength: 10,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(1, errorText: formErrorMinLength(1)),
                      FormBuilderValidators.maxLength(100, errorText: formErrorMaxLength(100)), // 수정이 필요함.
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'unitPrice',
                    decoration: StyleFormDecoration().getInputDecoration('횟수 / 요금'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn('완료', () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      PtTicketCreateRequest request = PtTicketCreateRequest(
                        _formKey.currentState!.fields['dateCreate']!.value,
                        _formKey.currentState!.fields['storeMember']!.value,
                        _formKey.currentState!.fields['trainerMember']!.value,
                        _formKey.currentState!.fields['ticketName']!.value,
                        _formKey.currentState!.fields['maxCount']!.value,
                        _formKey.currentState!.fields['unitPrice']!.value,
                      );
                      _setPtTicket(request);
                    }
                  }),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
