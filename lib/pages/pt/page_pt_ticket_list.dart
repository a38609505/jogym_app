import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PagePtTicketList extends StatelessWidget {
  const PagePtTicketList({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const ComponentAppbarPopup(
          title: "PT권 정보 관리",
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: 150,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      '+ 등록',
                      () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => const PageLogin()),
                      ),
                      bgColor: const Color.fromRGBO(21, 161, 212, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(
                enumSize: EnumSize.small,
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "1",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 100,
                            height: 150,
                            child: Image.asset("assets/roni.png"),
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "2",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "그럴싸한 계획.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 100,
                            height: 150,
                            child: Image.asset("assets/tison.png"),
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "3",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "코너에 몰리면.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 100,
                            height: 150,
                            child: Image.asset("assets/mac.png"),
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "4",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "실버가누.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: const Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 100,
                            height: 150,
                            child: Image.asset("assets/silver.png"),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
