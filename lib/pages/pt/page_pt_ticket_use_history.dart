import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_buy_history.dart';
import 'package:jo_gym_app/pages/season/page_season_buy_history.dart';

class PagePtTicketUseHistory extends StatelessWidget {
  const PagePtTicketUseHistory({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        appBar: const ComponentAppbarPopup(title: "PT권 이용 내역"),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 180,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      '정기권',
                          () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageSeasonBuyHistory()),
                      ),
                      bgColor: const Color.fromRGBO(49, 176, 121, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                  const ComponentMarginHorizon(enumSize: EnumSize.mid),
                  Container(
                    width: 180,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      'PT권',
                          () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PagePtTicketBuyHistory()),
                      ),
                      bgColor: const Color.fromRGBO(49, 176, 121, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                ],
              ),

              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                child: Column(
                  children: [
                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 300,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "사용일시",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "2023-01-02 10:20",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "트레이너",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "로니콜먼",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "20",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "사용횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "10",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "잔여횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "10",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),

                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 300,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "2",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "사용일시",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "2023-01-02 18:20",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "트레이너",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "로니콜먼",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "20",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "사용횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "11",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "잔여횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "9",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),

                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 300,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "3",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "사용일시",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "2023-01-03 18:20",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "트레이너",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "로니콜먼",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "20",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "사용횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "12",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "잔여횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "8",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
