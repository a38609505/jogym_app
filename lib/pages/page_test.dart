import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';

class PageTest extends StatelessWidget {
  const PageTest({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "Test Page"),
      body: Text("Test Page"),

    );
  }
}
