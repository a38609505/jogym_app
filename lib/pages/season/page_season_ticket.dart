import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/common/component_text_icon_full_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PageSeasonTicket extends StatelessWidget {
  const PageSeasonTicket({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarPopup(title: "회원권 구매"),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 180,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      '정기권',
                      () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => PageLogin()),
                      ),
                      bgColor: Color.fromRGBO(49, 176, 121, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                  const ComponentMarginHorizon(enumSize: EnumSize.mid),
                  Container(
                    width: 180,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      'PT권',
                      () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => PageLogin()),
                      ),
                      bgColor: Color.fromRGBO(49, 176, 121, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                child: Column(
                  children: [
                    Container(
                        padding: bodyPaddingLeftRight,
                        width: 400,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 2),
                        ),
                        child: Row(
                          children: [
                            Container(
                              width: 200,
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Text(
                                          "No",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "1",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Text(
                                          "정기권명",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "1개월권",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Text(
                                          "월",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "1개월",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Text(
                                          "월 금액",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "100,000원/월",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Text(
                                          "총 금액",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "100,000원/월",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Stack(
                              children: [
                                Container(
                                  child: Image.asset("assets/ss.png"),
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.black, width: 1),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 33, top: 50),
                                  child: Text("정기권"),
                                ),
                              ],
                            ),
                          ],
                        )),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "2",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "정기권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "3개월권",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "월",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "3개월권",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "월 금액",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "100,000원/월",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "총 금액",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: const Text(
                                        "100,000원/월",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                              width: 30
                          ),
                          Stack(
                            children: [
                              Container(
                                child: Image.asset("assets/ss.png"),
                                margin: EdgeInsets.only(top: 10, bottom: 10),
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 33, top: 50),
                                child: Text("정기권"),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                        padding: bodyPaddingLeftRight,
                        width: 400,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 2),
                        ),
                        child: Row(
                          children: [
                            Container(
                              width: 200,
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: Text(
                                            "No",
                                            style: TextStyle(
                                              fontSize: fontSizeMid,
                                              fontWeight: FontWeight.w500,
                                            ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "3",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: Text(
                                            "정기권명",
                                            style: TextStyle(
                                              fontSize: fontSizeMid,
                                              fontWeight: FontWeight.w500,
                                            ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "3개월권",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: Text(
                                            "월",
                                            style: TextStyle(
                                              fontSize: fontSizeMid,
                                              fontWeight: FontWeight.w500,
                                            ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "3개월권",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: Text(
                                            "월 금액",
                                            style: TextStyle(
                                              fontSize: fontSizeMid,
                                              fontWeight: FontWeight.w500,
                                            ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "80,000원/월",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: Text(
                                            "총 금액",
                                            style: TextStyle(
                                              fontSize: fontSizeMid,
                                              fontWeight: FontWeight.w500,
                                            ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "240,000원/월",
                                          style: TextStyle(
                                            fontSize: fontSizeMid,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  ComponentMarginVertical(
                                    enumSize: EnumSize.small,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                                width: 30
                            ),
                            Stack(
                              children: [
                                Container(
                                  child: Image.asset("assets/ss.png"),
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.black, width: 1),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 33, top: 50),
                                  child: Text("정기권"),
                                ),
                              ],
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
