import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PageSeasonTicketChange extends StatefulWidget {
  const PageSeasonTicketChange({super.key});

  @override
  State<PageSeasonTicketChange> createState() => _PageSeasonTicketChangeState();
}

const double width = 300.0;
const double height = 60.0;
const double loginAlign = -1;
const double signInAlign = 1;
const Color selectedColor = Colors.white;
const Color normalColor = Colors.black54;

class _PageSeasonTicketChangeState extends State<PageSeasonTicketChange> {
  final _formKey = GlobalKey<FormBuilderState>();

  late double xAlign;
  late Color loginColor;
  late Color signInColor;
  String text = "";

  @override
  void initState() {
    super.initState();
    xAlign = loginAlign;
    loginColor = selectedColor;
    signInColor = normalColor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "PT권 정보 수정"),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                padding: const EdgeInsets.only(
                  bottom: 10,
                  left: 20,
                  right: 20,
                ),
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'sortation',
                          decoration:
                              StyleFormDecoration().getInputDecoration('구분'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'seasonTicketName',
                          decoration:
                              StyleFormDecoration().getInputDecoration('정기권명'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'month',
                          decoration:
                              StyleFormDecoration().getInputDecoration('월'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(1,
                                errorText: formErrorMinLength(1)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'monthPrice',
                          decoration:
                              StyleFormDecoration().getInputDecoration('월 요금'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(5,
                                errorText: formErrorMinLength(5)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '수정 / 저장',
                          () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageLogin()),
                        ),
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                      ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '삭제',
                              () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageLogin()),
                          ),
                          bgColor: Color.fromRGBO(84, 89, 87, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
