import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/model/season_ticket_create_request.dart';
import 'package:jo_gym_app/pages/page_main.dart';
import 'package:jo_gym_app/repository/repo_season_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PageSeasonTicketCreate extends StatefulWidget {
  const PageSeasonTicketCreate({super.key});

  @override
  State<PageSeasonTicketCreate> createState() => _PageSeasonTicketCreateState();
}

class _PageSeasonTicketCreateState extends State<PageSeasonTicketCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  String? option;

  Future<void> _setTicketSeason(SeasonTicketCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoSeasonTicket().setSeasonTicket(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '정기권 등록 완료',
        subTitle: '정기권 등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '정기권 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '정기권 정보 등록'),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingAll,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDateTimePicker(
                    name: "dateCreate",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    decoration: StyleFormDecoration().getDateBoxDecoration("등록일시"),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'storeMember',
                    decoration: StyleFormDecoration().getInputDecoration('가맹점'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDropdown<String>(
                    name: 'ticketType',
                    decoration: const InputDecoration(
                      label: Text('구분'),
                    ),
                    onChanged: (value) {
                      setState(() {
                        option = value;
                      });
                    },
                    items: const [
                      DropdownMenuItem(value: "MONTH", child: Text('정기권')),
                      DropdownMenuItem(value: "DAY", child: Text('일일권')),
                    ],
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    obscureText: true,
                    name: 'ticketName',
                    decoration:
                    StyleFormDecoration().getInputDecoration('정기권명'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'maxMonth',
                    decoration:
                    StyleFormDecoration().getInputDecoration('월'),
                    maxLength: 10,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(1, errorText: formErrorMinLength(1)),
                      FormBuilderValidators.maxLength(10, errorText: formErrorMaxLength(10)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'unitPrice',
                    decoration: StyleFormDecoration().getInputDecoration('횟수 / 요금'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn('완료', () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      SeasonTicketCreateRequest request = SeasonTicketCreateRequest(
                        _formKey.currentState!.fields['dateCreate']!.value,
                        _formKey.currentState!.fields['storeMember']!.value,
                        _formKey.currentState!.fields['ticketType']!.value,
                        _formKey.currentState!.fields['ticketName']!.value,
                        _formKey.currentState!.fields['maxMonth']!.value,
                        _formKey.currentState!.fields['unitPrice']!.value,
                      );
                      _setTicketSeason(request);
                    }
                  }),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
