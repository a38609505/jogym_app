import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/page_login.dart';

class PageSeasonTicketList extends StatelessWidget {
  const PageSeasonTicketList({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarPopup(title: "회원권 구매"),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 180,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      '정기권',
                          () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageLogin()),
                      ),
                      bgColor: Color.fromRGBO(49, 176, 121, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                  const ComponentMarginHorizon(enumSize: EnumSize.mid),
                  Container(
                    width: 180,
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      'PT권',
                          () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageLogin()),
                      ),
                      bgColor: Color.fromRGBO(49, 176, 121, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                child: Column(
                  children: [
                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          Stack(
                            children: [
                              Container(
                                child: Image.asset("assets/ss.png"),
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 30, top: 38),
                                child: Text("정기권"),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          Stack(
                            children: [
                              Container(
                                child: Image.asset("assets/ss.png"),
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 30, top: 38),
                                child: Text("정기권"),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          Stack(
                            children: [
                              Container(
                                child: Image.asset("assets/ss.png"),
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 30, top: 38),
                                child: Text("정기권"),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                    Container(
                      padding: bodyPaddingLeftRight,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 200,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "No",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "PT권명",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "옆집 로니.avi",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "횟수",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "50회",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      child: Text(
                                        "단위요금",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "1,000,000,000원",
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 50,
                          ),
                          Stack(
                            children: [
                              Container(
                                child: Image.asset("assets/ss.png"),
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 1),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 30, top: 38),
                                child: Text("정기권"),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const ComponentMarginVertical(
                      enumSize: EnumSize.mid,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
