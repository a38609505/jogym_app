import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/middleware/middleware_login_check.dart';
import 'package:jo_gym_app/model/login_request.dart';
import 'package:jo_gym_app/repository/repo_member.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({super.key});
  @override
  State<PageLogin> createState() => _PageLoginState();
}

const double width = 300.0;
const double height = 60.0;
const double loginAlign = -1;
const double signInAlign = 1;
const Color selectedColor = Colors.white;
const Color normalColor = Colors.black54;

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  late double xAlign;
  late Color loginColor;
  late Color signInColor;
  String text = "";


  @override
  void initState() {
    super.initState();
    xAlign = loginAlign;
    loginColor = selectedColor;
    signInColor = normalColor;
  }

  Future<void> _doLogin(LoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }




  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: ComponentAppbarPopup(title: "로그인 페이지"),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Container(
                  child: Text(
                    "운동 업체 관리 프로그램 APP",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              const ComponentMarginVertical(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                color: Colors.white,
                child: Image.asset(
                  "assets/spologo.png",
                  fit: BoxFit.cover,
                ),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                padding: const EdgeInsets.only(
                  bottom: 10,
                  left: 20,
                  right: 20,
                ),
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'username',
                          decoration: StyleFormDecoration().getInputDecoration('아이디'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(errorText: formErrorRequired),
                            FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                            FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          obscureText: true, // 비밀번호 암호화 처리
                          name: 'password',
                          decoration: StyleFormDecoration().getInputDecoration('비밀번호'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(errorText: formErrorRequired),
                            FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                            FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn('로그인', () {
                          if(_formKey.currentState!.saveAndValidate()) {
                            LoginRequest loginRequest = LoginRequest( // LoginRequest에 입력받은 아이디, 비밀번호를 넘겨준다
                              _formKey.currentState!.fields['username']!.value,
                              _formKey.currentState!.fields['password']!.value,
                            );
                            _doLogin(loginRequest);
                          }
                        }),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
