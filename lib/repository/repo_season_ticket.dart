import 'package:dio/dio.dart';
import 'package:jo_gym_app/config/config_api.dart';
import 'package:jo_gym_app/model/login_request.dart';
import 'package:jo_gym_app/model/login_result.dart';
import 'package:jo_gym_app/model/season_ticket_create_request.dart';

class RepoSeasonTicket {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUri/member/login/app/admin';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }
  setSeasonTicket(SeasonTicketCreateRequest request) {}
}