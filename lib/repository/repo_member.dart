import 'package:dio/dio.dart';
import 'package:jo_gym_app/config/config_api.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/license_update_request.dart';
import 'package:jo_gym_app/model/login_request.dart';
import 'package:jo_gym_app/model/login_result.dart';
import 'package:jo_gym_app/model/member_create_request.dart';
import 'package:jo_gym_app/model/member_join_request.dart';
import 'package:jo_gym_app/model/member_password_find_request.dart';
import 'package:jo_gym_app/model/my_profile_status_item.dart';
import 'package:jo_gym_app/model/my_profile_status_item_result.dart';
import 'package:jo_gym_app/model/password_change_request.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUri/member/login/app/user';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<CommonResult> doJoin(MemberJoinRequest request) async {
    const String baseUrl = '$apiUri/member/join';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doPasswordFind(MemberPasswordFindRequest request) async {
    const String baseUrl = '$apiUri/member-info/find-password';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doPasswordChange(PasswordChangeRequest request) async {
    const String baseUrl = '$apiUri/member-info/change-password';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doLicence(LicenseUpdateRequest licenseUpdateRequest) async {
    const String baseUrl = '$apiUri/member-info/licence';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: licenseUpdateRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<MyProfileStatusItemResult> getList() async {
    final String baseUrl = '$apiUri/member-info/my/profile';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MyProfileStatusItemResult.fromJson(response.data);
  }
  setMember(MemberCreateRequest request) {}
}
Future<CommonResult> doJoin(MemberJoinRequest request) async {
  const String baseUrl = '$apiUri/member/join';

  Dio dio = Dio();

  final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
  );

  return CommonResult.fromJson(response.data);
}

