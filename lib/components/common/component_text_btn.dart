import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';

class ComponentTextBtn extends StatelessWidget {
  final String text;
  final Color bgColor;
  final Color textColor;
  final Color borderColor;
  final VoidCallback callback;

  const ComponentTextBtn(this.text, this.callback, {Key? key, this.bgColor = colorPrimary, this.textColor = Colors.white, this.borderColor = colorPrimary}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: callback,
      child: Text(text),
      style: ElevatedButton.styleFrom(
          primary: bgColor,
          onPrimary: textColor,
          padding: contentPaddingButton,
          elevation: buttonElevation,
          textStyle: const TextStyle(
            fontSize: fontSizeMid,
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(buttonRadius),
              side: BorderSide(
                color: borderColor,
              )
          )
      ),
    );
  }
}
