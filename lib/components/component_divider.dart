import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_color.dart';

class ComponentDivider extends StatelessWidget {
  const ComponentDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const Divider(
        thickness: 2,
        height: 1,
        color: colorDarkGray,
        indent: 10,
        endIndent: 10);
  }
}
